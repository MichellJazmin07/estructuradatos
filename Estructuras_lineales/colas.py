"""
by: RAy Parra
Colas, son estructuras de datos estaticas y dinamicas, que permiten manejar datos en serie
son utilizadas en diferentes areas, en especial en lo industrial, todo esta basado en la 
teoria de colas, existen colas simples, dobles, circulares, multiples, servidor y cliente
entre otras.

ejemplo: 

     1, 2, 3, 4, 5   --> orden de entrada
cola[4, 1, 7, 2, 9]  --> orden de salida: 4, 1, 7, 2, 9 
     0, 1, 2, 3, 4   --> indices

los metodos para utilizar
push()
pop()
inicio()
final()
read()
"""
import random

class Cola(object):
    def __init__(self):
        self.cola = [0, 0, 0, 0, 0]
        self.t = len(self.cola)
        self.b = 0
        self.p = 0

    def push(self, element):
        if self.p>=self.t:
            print("Cola Llena")
            self.p -= 1
        else:
            if self.cola[self.p]!=0:
                print("Indice no Disponible o Cola Llena!")
            else:
                print(f"valor de pu:{self.p}")
                self.cola[self.p] = element
                self.p += 1

    def pop2(self):
        if (self.p<=self.b) and (self.cola[self.p]==0):
                print("Cola Vacia")
        else:
            self.cola[0] = 0
            del(self.cola[0])
            self.cola.append(0)
            self.p -=1
            
            if self.p<0:
                self.p += 1
            print(f"Valor de Po {self.p}")

    def peek(self):
        return self.cola[0]

    def read(self):
        for i in self.cola:
            print(f"Item: {i}")

cola_choice = ["AC/DC", "RAMSTEIN", "METALLICA", "SCORPIONS", "CAIFANES", "TIERRA SANTA", "HEROES DEL SILENCIO"]


cola = Cola()

print(f"Longitud de la Cola: {cola.t}")
cola.push(random.choice(cola_choice))
print(f"Elementos en la Cola: {cola.cola}")
print(f"Puntero de la Cola: {cola.p}")
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.read()

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.read()

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.read()


















