import random

class Cola(object):
    def __init__(self):
        self.cola = []
        self.t = len(self.cola)
        self.b = 0
        self.p = 0

    def push(self, element):
        self.cola.append(element)
        self.p += 1

    def pop2(self):
        if self.cola==[]:
            print("Cola Vacia!")
        else:
            if (self.p<=self.b) and (self.cola[self.p]==0):
                    print("Cola Vacia")
            else:
                self.cola.reverse()
                self.cola.pop()
                self.cola.reverse()
                self.p -= 1
                if self.p<0:
                    self.p += 1
                print(f"Valor de Po {self.p}")

    def peek(self):
        if self.cola==[]:
            return "Cola Vacia!"
        else:
            return self.cola[0]

    def read(self):
        for i in self.cola:
            print(f"Item: {i}")

cola_choice = ["AC/DC", "RAMSTEIN", "METALLICA", "SCORPIONS", "CAIFANES", "TIERRA SANTA", "HEROES DEL SILENCIO"]


cola = Cola()

print(f"Longitud de la Cola: {cola.t}")
cola.push(random.choice(cola_choice))
print(f"Elementos en la Cola: {cola.cola}")
print(f"Puntero de la Cola: {cola.p}")
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.read()

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.push(random.choice(cola_choice))
print(cola.cola)
print(cola.p)
print(f"Peek: {cola.peek()}")

cola.read()

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.pop2()
print(cola.cola)
print(f"Peek: {cola.peek()}")

cola.read()


