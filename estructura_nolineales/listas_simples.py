"""
Las listas enlazadas son estructuras no lineales que almacenan informacion en memoria dinamica,
se utilizan principalmente en la solucion de problemas que implican el uso de arboles, son empleadas
comunmente en algoritmos de busqueda y ordenamiento. Un ejemplo claro, es el control-z que se utiliza en
el sistema operativo.
Las listas contienen, una raiz o base, algunos autores les dicen cabeza de la lista, contienen los nodos
concecuentes y contiene un final o cola. Ademas utilizamos los punteros p y q para controlar el incremento o 
decremento de la lista.
Los metodos son:
insertar nodo
eliminar nodo
leer(recorrer) la lista
buscar
"""

class Node(object):
    def __init__(self, data=None, link=None):
        self.data = data
        self.link = link
    
    def __str__(self):
        return self.data


class Lista_simple(object):
    def __init__(self):
        self.raiz = None
        self.cola = None
    
    def add_node(self, nodo):
        if self.raiz==None:
            self.raiz = nodo
        if self.cola!=None:
            self.cola.link = nodo
        self.cola = nodo
    
    def read(self):
        p = self.raiz
        while p != None:
            print(p.data)
            p = p.link

    def find_node(self, dato, find_data=False):
        p = self.raiz
        while find_data==False and p.link!=None:
            if p.data==dato:
                print(f"Dato: {dato} encontrado!")
                find_data = True
            p = p.link
    
    def left_add(self, dato, band, find_data=False):
        p = self.raiz
        q = self.raiz
        r = self.raiz
        while find_data==False and p.link!=None:
            if p.data==dato:
                find_data = True
                r = Node(band)
                q.link = r
                r.link = p
            else:
                q = p

            p = p.link

    def right_add(self, dato, band, find_data=False):
        p = self.raiz
        q = self.raiz
        r = self.raiz
        while find_data==False and p.link!=None:
            if p.data==dato:
                find_data = True
                r = Node(band)
                q = p.link
                p.link = r
                r.link = q
            else:
                q = p

            p = p.link





data_choice = ["AC/DC", "RAMSTEIN", "METALLICA", "SCORPIONS", "CAIFANES", "TIERRA SANTA", "HEROES DEL SILENCIO"]
ls = Lista_simple()

for band in data_choice:
    node = Node(band)
    ls.add_node(node)
    
ls.read()

# banda = str(input("Escribe la banda que deseas buscar: "))
# ls.find_node(banda.upper())

dato = str(input("Escribe la banda donde quieres insertar un nodo a la izquierda: "))
banda = str(input("Escribe la banda que deseas buscar: "))
ls.left_add(dato.upper(), banda.upper())
ls.read()

dato = str(input("Escribe la banda donde quieres insertar un nodo a la derecha: "))
banda = str(input("Escribe la banda que deseas buscar: "))
ls.right_add(dato.upper(), banda.upper())
ls.read()

"""
r; p,q = r;
r            q p
R  ------->   A  ->  B  ->  C
"""

# r = Node("Raiz")
# p = r
# q = r

# p = Node("A")
# q.link = p


# print(q.data)
# q = p
# print(q.data)












